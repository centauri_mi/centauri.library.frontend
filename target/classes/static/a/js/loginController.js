app.controller('loginController', function($rootScope, $scope,
            $http, $location, $route){
//$rootScope.authenticated = false ;
//    console.log("LOGIN CONTROLLER $rootScope.authenticated: " + $rootScope.authenticated);
//    $scope.credentials = {username:"a@a.com", password:"test"};
    $rootScope.adminUser=false;
	$rootScope.is_visible = false;

    $scope.resetForm = function() {
            $scope.credentials = null;
    }
    var authenticate = function(credentials, callback) {
            var headers = $scope.credentials ? {
                    authorization : "Basic "
                                    + btoa($scope.credentials.username + ":"
                                                    + $scope.credentials.password)
                    } : {};
            $http.get(AUTHENTICATION_URI, {
                    headers : headers
            }).then(function(response) {
            	console.log("response.data.name: " + response.data);
                    if (response.data.name) {
                            $rootScope.authenticated = true;
                                 console.log("$rootScope.authenticated 1"+ $rootScope.authenticated)                        
                            $http({
                        		method : 'GET',
                        		url : AUTHENTICATION_BACKEND_GETUSER_BY_EMAIL_URI+$scope.credentials.username
                        	}).then(function(response) {
                        		console.log(response.data);
                        		$rootScope.userLogged = response.data;
                        		$rootScope.is_visible = true;
                        		console.log("Utente "+$rootScope.userLogged.id+" loggato!");
                        		console.log("L'utente ha ruolo: "+$rootScope.userLogged.role);
                        		if ($rootScope.userLogged.role == 0){
                        			$rootScope.adminUser=true;
                        		}
                        	});
                                                           
                    } else {
                            $rootScope.authenticated = false;
                            console.log("$rootScope.authenticated 2"+ $rootScope.authenticated)   
                    }
                    callback && callback();
            }, function() {
                    $rootScope.authenticated = false;
                    console.log("$rootScope.authenticated 1"+ $rootScope.authenticated)   
                    callback && callback();
            });
    }
    $scope.loginUser = function() {
          authenticate($scope.credentials, function() {
              if ($rootScope.authenticated) {
                $location.path("/");
                $scope.loginerror = false;
              } else {
                $location.path("/login");
                $scope.loginerror = true;
              }
         });
    };
});