var app = angular.module('userregistrationsystem', [ 'ngRoute', 'ngResource' ]);

app.config(function($routeProvider) {
	$routeProvider.when('/list-all-books-to-download', {
		templateUrl : './template/book/listb.html',
		controller : 'listBookController'
	}).when('/login', {
		templateUrl : './template/login.html',
		controller : 'loginController'
	}).when('/logout', {
		templateUrl : './template/login.html',
		controller : 'logoutController'
	}).when('/register-new-user', {
		templateUrl : './template/userregistration.html',
		controller : 'registerUserController'
	}).otherwise({
//		home
		redirectTo : '/list-all-books-to-download',
		templateUrl : './template/book/listb.html',
		controller : 'listBookController'
	});
	

});

// ################# START SERVICE NOTICE OPERATION #################
var timeNotice = 2000; // time in milliseconds (1000 = 1sec)

app.service('notice', function($rootScope) {
	
	this.success = function noticeSuccess() {
		console.log("noticeSuccess invoked");
		$rootScope.successMessageBool = true;
		setTimeout(function() {
			$('#noticeModalSuccess').fadeOut('fast');
			$rootScope.successMessageBool = false;
		}, timeNotice);
	}

	this.error = function noticeError(msg) {
		console.log("noticeError invoked");
		console.log("error message received: "+msg);
		$rootScope.errorMessage = msg;
		$rootScope.errorMessageBool = true;
		console.log("erorreorererr"+$rootScope.errorMessageBool)
		setTimeout(function() {
			$('#noticeModalError').fadeOut('fast');
			$rootScope.errorMessageBool = false;
			console.log("erorreorererr"+$rootScope.errorMessageBool)
			$rootScope.errorMessage = "";
		}, timeNotice);
	}
	
	this.warn = function noticeWarn(msg) {
		console.log("noticeWarn invoked");
		console.log("warn message received: "+msg);
		$rootScope.warnMessage = msg;
		$rootScope.warnMessageBool = true;
		setTimeout(function() {
			$('#noticeModalWarn').fadeOut('fast');
			$rootScope.warnMessageBool = false;
			$rootScope.warnMessage = "";
		}, timeNotice);
	}
	
	this.database = function noticeErrorDB() {
		console.log("noticeErrorDB invoked");
		$rootScope.errorDBMessageBool = true;
		setTimeout(function() {
			$('#noticeModalErrorDB').fadeOut('fast');
			$rootScope.errorDBMessageBool = false;
		}, timeNotice);
	}
});
//################# END SERVICE NOTICE OPERATION #################

// DIRECTIVE - FILE MODEL
app.directive('fileModel', ['$parse', function ($parse) {
    return {
       restrict: 'A',
       link: function(scope, element, attrs) {
          var model = $parse(attrs.fileModel);
          var modelSetter = model.assign;
           
          element.bind('change', function(){
             scope.$apply(function(){
                modelSetter(scope, element[0].files[0]);
             });
          });
       }
    };
     
}]);

// DIRECTIVE - TOOLTIP

app.directive('toggle', function(){
  return {
    restrict: 'A',
    link: function(scope, element, attrs){
      if (attrs.toggle=="tooltip"){
        $(element).tooltip();
      }
      if (attrs.toggle=="popover"){
        $(element).popover();
      }
    }
  };
})