
		

app.controller('listBookController', function (notice, $scope, $http, $location, $route, $interval, $rootScope) {

	$scope.logged = false;
	
	if ($rootScope.authenticated) {
	       $location.path("/");
	       $scope.loginerror = false;
	       $scope.logged = true;
	    } else {
	       $location.path("/login");
	       $scope.loginerror = true;
	      
	}
	
    $scope.thumbImgFullPath = thumbImgFullPath ;
    function listItems() {
        console.log("list invoked");
        Pace.restart();
        $http({
            method: 'GET',
            url: bookResourceApi
           
        }).then(function (response) {
            $scope.list = response.data;
            console.log($scope.list); 
        });
    }
    $scope.updateBook = function (bookId) {
        $location.path("/update-book/" + bookId);
    }
    $scope.deleteBook = function (bookIsbn) {
        $http({
            method: 'DELETE',
            url: bookResourceApi + bookIsbn
        }).then(function (response) {
                $location.path("/list-all-books");
                console.log(response);
                notice.success();
                reload();

            },
            // Error
            function (response) {
                $scope.uploadResult = response.data;
                console.log(response);
                if (response.status == 500)
                    notice.database();
                else if (response.status == 409){
                	$location.path("/list-all-books");
                    console.log(response);
                    notice.warn(response.data.errorMessage);
                    reload();
                } else
                    notice.error(response.data.errorMessage);

            });
    }

    $scope.getCandidates = function () {
        listItems();
    }

    $scope.nextPage = function () {
        listItems();
    };

    $scope.prevPage = function () {
        listItems();
    };

    //############initialization of the showed values
    $scope.data = {
        availableOptions: [{
                id: '1',
                value: '5',
                size: 5
            },
            {
                id: '2',
                value: '10',
                size: 10
            },
            {
                id: '3',
                value: '50',
                size: 50
            },
            {
                id: '4',
                value: '100',
                size: 100
            },
        ],
        selectedOption: {
            id: '1',
            value: '5',
            size: 5
        } //This sets the default value of the select in the ui
    };

    $scope.reloadBool = true;
    $scope.reloadCandidate = 0;
    

    function incrSec() {
        if ($scope.reloadBool) {
            //			console.log("autoReload: " + $scope.reloadBool);
            $scope.reloadCandidate += 10;
            if ($scope.reloadCandidate > 100) {
                $scope.reloadCandidate = 0;
                reload();
            }
        }
    }
    timer = $interval(incrSec, 1000);

    function reload() {
        $scope.reloadCandidate = 0;
        listItems();
    }
    $scope.listCand = function () {
        reload();
    }
    reload();
    $scope.autoReload = function autoReload() {
        if ($scope.reloadBool) {
            $scope.reloadBool = false;
            $(document).ready(function () {
                $('#candidatebar').addClass('progress-bar-reload-disabled');
            });
        } else {
            $scope.reloadBool = true;
            $(document).ready(function () {
                $('#candidatebar').removeClass('progress-bar-reload-disabled');
            });
        }
        console.log("autoReload: " + $scope.reloadBool);
    }
    $scope.$on("$destroy", function () {
        // clean up autoReload interval
        console.log("stopped Candidate autoReload: " + $interval.cancel(timer));
    });
});
